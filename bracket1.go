package main

import (
	"fmt"
)

func isBalanced(s string) string {
	matchingBracket := map[byte]byte{')': '(', ']': '[', '}': '{'}
	var stack []byte

	for i := 0; i < len(s); i++ {
		char := s[i]
		if char == '(' || char == '{' || char == '[' {
			stack = append(stack, char)
		} else if char == ')' || char == '}' || char == ']' {
			if len(stack) == 0 || stack[len(stack)-1] != matchingBracket[char] {
				return "NO"
			}
			stack = stack[:len(stack)-1]
		}
	}
	if len(stack) == 0 {
		return "YES"
	}
	return "NO"
}

func main() {
	// Test cases
	fmt.Println("========Test Case 1========")
	input1 := "{ [ ( ) ] }"
	fmt.Println("input = ", input1)
	fmt.Println("result = ", isBalanced(input1))
	fmt.Println("========End Test Case 1========")
	fmt.Println("========Test Case 2========")
	input2 := "{ [ ( ] ) }"
	fmt.Println("input = ", input2)
	fmt.Println("result = ", isBalanced(input2))
	fmt.Println("========End Test Case 2========")
	fmt.Println("========Test Case 3========")
	input3 := "{ ( ( [ ] ) [ ] ) [ ] }"
	fmt.Println("input = ", input3)
	fmt.Println("result = ", isBalanced(input3))
	fmt.Println("========End Test Case 3========")
}
