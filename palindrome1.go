package main

import (
	"fmt"
)

func solving(s string, k int) string {
	n := len(s)
	bytes := []byte(s)

	var makePalindrome func(l, r, k int) bool
	makePalindrome = func(l, r, k int) bool {
		if l >= r {
			return true
		}
		if bytes[l] == bytes[r] {
			return makePalindrome(l+1, r-1, k)
		}
		if k == 0 {
			return false
		}
		if bytes[l] > bytes[r] {
			bytes[r] = bytes[l]
		} else {
			bytes[l] = bytes[r]
		}
		return makePalindrome(l+1, r-1, k-1)
	}

	if !makePalindrome(0, n-1, k) {
		return "-1"
	}

	var maximizePalindrome func(l, r, k int)
	maximizePalindrome = func(l, r, k int) {
		if l >= r {
			return
		}
		if bytes[l] != '9' {
			if bytes[l] == bytes[r] && k >= 2 {
				bytes[l], bytes[r] = '9', '9'
				maximizePalindrome(l+1, r-1, k-2)
			} else if bytes[l] != bytes[r] && k >= 1 {
				if bytes[l] < bytes[r] {
					bytes[l] = '9'
				} else {
					bytes[r] = '9'
				}
				maximizePalindrome(l+1, r-1, k-1)
			} else {
				maximizePalindrome(l+1, r-1, k)
			}
		} else {
			maximizePalindrome(l+1, r-1, k)
		}
	}

	maximizePalindrome(0, n-1, k)
	return string(bytes)
}

func main() {
	fmt.Println("=======Test Case 1=======")
	s1 := "3943"
	k1 := 1
	fmt.Println("string = ", s1)
	fmt.Println("k =", k1)
	fmt.Println("result = ", solving(s1, k1))
	fmt.Println("=======End Test Case 1=======")
	fmt.Println("=======Test Case 2=======")
	s2 := "932239"
	k2 := 2
	fmt.Println("string = ", s2)
	fmt.Println("k =", k2)
	fmt.Println(solving(s2, k2))
	fmt.Println("=======End Test Case 2=======")
	fmt.Println("=======Test Case 3=======")
	s3 := "12321"
	k3 := 0
	fmt.Println("string = ", s3)
	fmt.Println("k =", k3)
	fmt.Println("result =", solving(s3, k3))
	fmt.Println("=======End Test Case 3=======")
}
