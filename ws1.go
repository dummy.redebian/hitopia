package main

import (
	"fmt"
)

func calculateWeight(s string) map[int]bool {
	weights := make(map[int]bool)
	length := len(s)

	for i := 0; i < length; {
		weight := int(s[i] - 'a' + 1)
		currentWeight := 0

		for j := i; j < length && s[j] == s[i]; j++ {
			currentWeight += weight
			weights[currentWeight] = true
		}

		for k := i + 1; k < length && s[k] == s[i]; k++ {
			i =k
		}
		i++
	}

	return weights
}

func checkQuery(weights map[int]bool, queries []int) []string{
	results := make([]string, len(queries))

	for i, query := range(queries) {
		if weights[query] {
			results[i] = "Yes"
		} else {
			results[i] = "No"
		}
	}

	return results
}

func main() {
	s1 := "abbcccd"
	queries1 := []int{1, 3, 9, 8}
	weights1 := calculateWeight(s1)
	results1 := checkQuery(weights1, queries1)
	fmt.Println("=====Test Case 1=====")
	fmt.Println("input string = ", s1)
	fmt.Println("query = ", queries1)
	fmt.Println("result =", results1)
	fmt.Println("=====End Test Case 1=====")

	s2 := "aabbbbcc"
	queries2 := []int{1, 4, 8, 12, 3}
	weights2 := calculateWeight(s2)
	results2 := checkQuery(weights2, queries2)
	fmt.Println("=====Test Case 2=====")
	fmt.Println("input string = ", s2)
	fmt.Println("query = ", queries2)
	fmt.Println("result =", results2)
	fmt.Println("=====End Test Case 2=====")

	s3 := "xyz"
	queries3 := []int{24, 25, 26, 51, 49}
	weights3 := calculateWeight(s3)
	results3 := checkQuery(weights3, queries3)
	fmt.Println("=====Test Case 3=====")
	fmt.Println("input string = ", s3)
	fmt.Println("query = ", queries3)
	fmt.Println("result =", results3)
	fmt.Println("=====End Test Case 3=====")
}